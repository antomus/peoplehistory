package com.example.antomus.peoplesearch;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;


public class DetailsActivity extends ActionBarActivity {

    String name;
    String story;
    String status;
    String birthPlace;
    String livingPlace;
    String source;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // получаем Intent, который вызывал это Activity
        Intent intent = getIntent();
//        name = intent.getStringExtra("name");
//        story = intent.getStringExtra("story");
//        status = intent.getStringExtra("status");
//        birthPlace = intent.getStringExtra("birthPlace");
//        livingPlace = intent.getStringExtra("livingPlace");
//        source = intent.getStringExtra("source");
        final TextView name = (TextView) findViewById(R.id.name);
        final TextView story = (TextView) findViewById(R.id.story);
        final TextView status = (TextView) findViewById(R.id.status);
        final TextView birthPlace = (TextView) findViewById(R.id.birthPlace);
        final TextView livingPlace = (TextView) findViewById(R.id.livingPlace);
        final TextView source = (TextView) findViewById(R.id.source);
        name.setText(intent.getStringExtra("name"));
        story.setText(intent.getStringExtra("story"));
        status.setText(intent.getStringExtra("status"));
        birthPlace.setText(intent.getStringExtra("birthPlace"));
        livingPlace.setText(intent.getStringExtra("livingPlace"));
        source.setText(intent.getStringExtra("source"));

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.menu_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
