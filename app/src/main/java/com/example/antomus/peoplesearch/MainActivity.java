package com.example.antomus.peoplesearch;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.SearchView;
import android.widget.Toast;


public class MainActivity extends Activity {
    EditText multiCity;
    EditText multiPeople;
    CheckBox checkBox;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button search_button = (Button) findViewById(R.id.search_button);
        Button refresh_button = (Button) findViewById(R.id.refresh_button);
        checkBox = (CheckBox) findViewById(R.id.checkBox);
        multiCity = (EditText) findViewById(R.id.multiCity);
        multiCity.setImeOptions(EditorInfo.IME_ACTION_DONE);
        multiPeople = (EditText) findViewById(R.id.multiPeople);
        multiPeople.setImeOptions(EditorInfo.IME_ACTION_DONE);

        search_button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if(!checkBox.isChecked()) {
                    Log.e("MainAct", multiPeople.getText().toString());
                    if(multiCity.getText().toString().isEmpty() && multiPeople.getText().toString().isEmpty()) {
                        Toast toast = Toast.makeText(MainActivity.this, "Будь ласка заповніть хоч одне поле", Toast.LENGTH_LONG);
                        View tview = toast.getView();
                        toast.setGravity(Gravity.TOP|Gravity.FILL_HORIZONTAL, 0, 0);
                        tview.setBackgroundColor(0xccc);
                        toast.show();
                    } else {
                        Intent intent = new Intent(getBaseContext(), ResultActivity.class);
                        intent.setAction("LocalSearch");
                        Log.e("MainActivity onClick", multiCity.getText().toString());
                        intent.putExtra("name", multiPeople.getText().toString());
                        intent.putExtra("place", multiCity.getText().toString());
                        startActivity(intent);
                    }
                } else {
                    Intent intent = new Intent(getBaseContext(), ResultActivity.class);
                    intent.setAction("LocalSearch");
                    intent.putExtra("showAll", true);
                    startActivity(intent);
                }


            }

        });

        refresh_button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), InitialLoadActivity.class);
                intent.setAction(Intent.ACTION_SEND);
                intent.putExtra("DataRefresh",true);
                startActivity(intent);
            }



        });

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = ((CheckBox) v).isChecked();
                multiCity.setEnabled(!checked);
                multiCity.setClickable(!checked);
                multiPeople.setEnabled(!checked);
                multiPeople.setClickable(!checked);
            }
        });

        //handleIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            doMySearch(query);
        }
    }

    private void doMySearch(String query) {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, query, duration);
        toast.show();
    }

    public void onCheckboxClicked(View view) {
        boolean checked = ((CheckBox) view).isChecked();
        multiCity.setEnabled(!checked);
        multiPeople.setEnabled(!checked);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
//        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
//        // Assumes current activity is the searchable activity
//        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
       // searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default

        return true;
        //return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }
}
