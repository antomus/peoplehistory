package com.example.antomus.peoplesearch.models;

import com.orm.SugarRecord;

/**
 * Created by antomus on 3/30/15.
 */
public class Place extends SugarRecord<Place> {
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public int getPlaceId() {
        return id;
    }

    public void setPlaceId(int id) {
        this.id = id;
    }

    int id;
    String title;
    String date;

    public Place(){
    }

    public Place(int id, String title, String date){
        this.id = id;
        this.title = title;
        this.date = date;

    }

    public String toString(){
        return this.title;
    }
}
