package com.example.antomus.peoplesearch;

import com.example.antomus.peoplesearch.utils.MyLifecycleHandler;

import net.danlew.android.joda.JodaTimeAndroid;

/**
 * Created by antomus on 4/4/15.
 */
public class MyApplication extends com.orm.SugarApp{
    @Override
    public void onCreate() {
        super.onCreate();
        JodaTimeAndroid.init(this);
        registerActivityLifecycleCallbacks(new MyLifecycleHandler());
    }
}
