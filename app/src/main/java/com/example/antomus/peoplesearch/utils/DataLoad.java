package com.example.antomus.peoplesearch.utils;

import android.util.Log;


import com.example.antomus.peoplesearch.models.Place;
import com.example.antomus.peoplesearch.models.Post;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import net.sf.junidecode.Junidecode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;

/**
 * DataLoad is an utility class responsible for fetching data from remote server and saving it to db.
 */

public class DataLoad {

    static OkHttpClient client = new OkHttpClient();
    static CookieManager cookieManager = new CookieManager();
    static String baseUrl = "http://www.localhistory.org.ua/";
    public static void login() throws IOException {
        String url = "http://www.localhistory.org.ua/wp-login.php";
        //MyApplication.cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        //client.setCookieHandler(MyApplication.cookieManager);
        //CookieManager cookieManager = new CookieManager();
        if(cookieManager.getCookieStore().getCookies().size() > 0) return;
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        client.setCookieHandler(cookieManager);
        String cookies = cookieManager.getCookieStore().getCookies().toString();
        Log.e("COOKIES login: ", cookies);
        RequestBody formBody = new FormEncodingBuilder()
            .add("log", "[user]")
            .add("pwd", "[password]")
            .add("wp-submit", "Вхід")
            .add("redirect_to", "http://www.localhistory.org.ua/wp-admin/")
            .add("testcookie", "1")
            .build();
        Request request = new Request.Builder()
            .url(url)
            .post(formBody)
            .build();

        Response response = client.newCall(request).execute();
        Log.e("RESPONSE", response.body().string());
    }

    public static void getPosts() throws IOException, JSONException {
        Log.e("getPosts","here");
        String url = baseUrl+"?json=get_posts&post_type=peoples&count=-1&orderby=date&order=ASC";
        //cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        //client.setCookieHandler(MyApplication.cookieManager);

        String cookies = cookieManager.getCookieStore().getCookies().toString();
        Log.e("COOKIES getPosts: ", cookies);
        Request request = new Request.Builder()
            .url(url)
            .build();

        Response response = client.newCall(request).execute();
        //Log.i("PEOPLE", response.body().string());
        savePosts(response.body().string());

    }

    private static void savePosts(String json) throws JSONException{
        JSONObject jObject = new JSONObject(json);
        JSONArray posts = jObject.getJSONArray("posts");
        Post.executeQuery("DELETE FROM POST");
        Post.executeQuery("VACUUM");
        /*
        custom_fields
        lh_source: ["Пастернак Р. Не вмре слава героїв. – Перемишляни, 2000. – Вип. 2."]
lh_status: ["Стрілець УПА, шеф штабу куреня «Гонти» з політпропаганди."]
lh_story: [" Загинув 09.09.1944 р. в урочищі Ріпище біля с. Стоки."]

        lh_birth_place: ["Нові Стрілища"]
lh_living_place: ["Нові Стрілища"]
lh_source: ["Пастернак Р. Не вмре слава героїв. – Перемишляни, 2000. – Вип. 2."]
lh_status: ["Стрілець УПА, командир куреня особливого призначення ВО-2 УПА-Захід."]
lh_story: ["Загинув 09.09.1944 р. в урочищі Ріпище біля с. Стоки."]

         */

        for (int i = 0, postsLength = posts.length(); i < postsLength; i++) {
            JSONObject obj = posts.getJSONObject(i);
            JSONObject fields = obj.optJSONObject("custom_fields");
            String lh_source ="", lh_status= "", lh_story = "", lh_birth_place="", lh_living_place ="";
            if(fields.optJSONArray("lh_source") != null) {
                lh_source = (String)fields.optJSONArray("lh_source").get(0);
            }
            if(fields.optJSONArray("lh_status") != null) {
                lh_status = (String)fields.optJSONArray("lh_status").get(0);
            }
            if(fields.optJSONArray("lh_story") != null) {
                lh_story = (String)fields.optJSONArray("lh_story").get(0);
            }
            if(fields.optJSONArray("lh_birth_place") != null) {
                lh_birth_place = (String)fields.optJSONArray("lh_birth_place").get(0);
            }
            if(fields.optJSONArray("lh_living_place") != null) {
                lh_living_place = (String)fields.optJSONArray("lh_living_place").get(0);
            }
            Post post = new Post(obj.optInt("id"),
                                obj.optString("title"),
                    DataLoad.toAscii(obj.optString("title")),
                                lh_source,
                                lh_status,
                                lh_story,
                                lh_birth_place,
                    DataLoad.toAscii(lh_birth_place),
                                lh_living_place,
                    DataLoad.toAscii(lh_living_place),
                                obj.optString("date"));
            post.save();


        }
    }

    public static void getPlaces()throws IOException, JSONException{
        String url = baseUrl+"?json=get_posts&post_type=locations&count=-1&orderby=title&order=ASC";
        //cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        //client.setCookieHandler(MyApplication.cookieManager);
        String cookies = cookieManager.getCookieStore().getCookies().toString();
        Log.e("COOKIES getPlaces: ", cookies);
        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();
        //Log.i("PEOPLE", response.body().string());
        savePlaces(response.body().string());
    }

    public static void savePlaces(String json) throws JSONException {
        JSONObject jsonObj = new JSONObject(json);
        JSONArray places = jsonObj.optJSONArray("posts");
        if(places.length() > 0) Place.deleteAll(Place.class);
        for (int i = 0, placesSize = places.length(); i < placesSize; i++) {
            JSONObject obj = places.getJSONObject(i);
            Place place = new Place(obj.optInt("id"), obj.optString("title"), obj.optString("date"));
            place.save();
        }
    }


    public static String toAscii(String str){
        return Junidecode.unidecode(str);
    }


}