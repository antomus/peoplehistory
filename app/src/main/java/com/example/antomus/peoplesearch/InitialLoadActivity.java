package com.example.antomus.peoplesearch;


import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.example.antomus.peoplesearch.utils.NetworkTask;


public class InitialLoadActivity extends Activity {
    final String REFRESH_ACTION = "DataRefresh";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initial_load);
    }

    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        Boolean [] isRefresh = {false};
        if(intent != null && intent.getBooleanExtra(REFRESH_ACTION, false)) {
            isRefresh[0] = true;
        }
        Log.e("isRefresh Init ", isRefresh.toString());
        AsyncTask net = new NetworkTask(this);
        net.execute(isRefresh);
    }



}
