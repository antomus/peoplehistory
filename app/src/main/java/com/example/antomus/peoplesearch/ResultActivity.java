package com.example.antomus.peoplesearch;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.antomus.peoplesearch.models.Post;

import java.util.ArrayList;
import java.util.List;


public class ResultActivity extends ActionBarActivity {
    String name;
    String place;
    boolean showAll;
    List<Post> personsList = new ArrayList<>();
    final String  LOG_TAG = "ResultActivity";
    final String FROM_SEARCH_FORM = "LocalSearch";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // получаем Intent, который вызывал это Activity
        Intent intent = getIntent();
        showAll = intent.getBooleanExtra("showAll", false);
        name = intent.getStringExtra("name");
        place = intent.getStringExtra("place");

       Log.e("RESULT_ACTIVITY ","onCreate");

        if(intent.getAction() != FROM_SEARCH_FORM) {
            SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
            name = sharedPref.getString("name","");
            place = sharedPref.getString("place","");
            showAll = sharedPref.getBoolean("showAll", false);
        }

        ListView lvMain = (ListView)findViewById(R.id.people_list);
        lvMain.setEmptyView( findViewById(R.id.empty));
        if(showAll) {
            personsList = Post.find(Post.class,null,null,null,"title_ascii",null);
        } else {
            personsList = Post.getPostsFilteredByNameAndPlace(name, place);
        }

        Log.e("Res: personsList.size()", personsList.size() +"");
        lvMain.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, personsList));
        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Log.d(LOG_TAG, "itemClick: position = " + position + ", id = "
                        + id);
                Post data = personsList.get(position);
                Intent intent = new Intent(getBaseContext(), DetailsActivity.class);
                //Log.e("MainActivity onClick", multiCity.getText().toString().toLowerCase());

                intent.putExtra("name", data.getTitle());
                intent.putExtra("story", data.getStory());
                intent.putExtra("status", data.getStatus());
                intent.putExtra("birthPlace", data.getBirthPlace());
                intent.putExtra("livingPlace", data.getLivingPlace());
                intent.putExtra("source", data.getSource());
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_result, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {


        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
        // Save the user's current game state
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("name",name);
        editor.putString("place",place);
        editor.putBoolean("showAll",showAll);
        editor.commit();

//        savedInstanceState.putString("name", name);
//        savedInstanceState.putString("place", place);
    }

}
