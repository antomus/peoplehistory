package com.example.antomus.peoplesearch.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.example.antomus.peoplesearch.MainActivity;
import com.example.antomus.peoplesearch.models.Post;

import java.lang.ref.WeakReference;
import java.util.Calendar;
import java.util.Date;
import org.joda.time.DateTime;
import org.joda.time.Days;

import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by antomus on 3/30/15.
 */
public class NetworkTask extends AsyncTask<Boolean, Void, Void> {
    private Context mContext;
    WeakReference<Context> weakContext;
    public NetworkTask (Context context){

        weakContext = new WeakReference<>(context);
    }
    protected Void doInBackground(Boolean... params) {
        boolean isOldDb = false;
        Boolean isRefresh = params[0];
        Log.e("isRefresh ", isRefresh+"");
        if(weakContext.get() != null) {
            File db = new File(weakContext.get().getApplicationInfo().dataDir + "/databases/local_history_new.db");
            long lastMod = db.lastModified();
            Calendar today = Calendar.getInstance();
            today.clear(Calendar.HOUR); today.clear(Calendar.MINUTE); today.clear(Calendar.SECOND);
            Date todayDate = today.getTime();
            int days = Days.daysBetween(new DateTime(lastMod), new DateTime(todayDate)).getDays();
            isOldDb = days > 5;
        }

        try {
            if(!(Post.count(Post.class, null, null) > 0) || isOldDb || isRefresh) {
                DataLoad.login();
                try {
                    Thread.sleep(1000);                 //1000 milliseconds is one second.
                } catch(InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
                DataLoad.getPosts();
                DataLoad.getPlaces();
            }
        } catch (IOException e) {
            Log.e("network IOException", e.getMessage());
        } catch (JSONException e) {
            Log.e("network JSONException", e.getMessage());
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void data) {
        if(weakContext.get() != null && MyLifecycleHandler.isApplicationVisible()) {
            Intent i = new Intent(weakContext.get().getApplicationContext(), MainActivity.class);
            weakContext.get().startActivity(i);
            ((Activity) weakContext.get()).finish();
        }
    }



}
