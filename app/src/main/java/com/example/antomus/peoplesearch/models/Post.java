package com.example.antomus.peoplesearch.models;

import android.util.Log;

import com.example.antomus.peoplesearch.utils.DataLoad;
import com.orm.SugarRecord;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by antomus on 3/30/15.
 */
public class Post extends SugarRecord<Post> {
    int id;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public int getPostId() {
        return id;
    }

    public void setPostId(int id) {
        this.id = id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getLivingPlace() {
        return livingPlace;
    }

    public void setLivingPlace(String livingPlace) {
        this.livingPlace = livingPlace;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    String title;
    String titleAscii;
    String source;
    String status;
    String story;
    String birthPlace;
    String birthPlaceAscii;
    String livingPlace;
    String livingPlaceAscii;
    String date;

    public Post(){
    }

    public Post(int id,
                String title,
                String titleAscii,
                String source,
                String status,
                String story,
                String birthPlace,
                String birthPlaceAscii,
                String livingPlace,
                String livingPlaceAscii,
                String date){
        this.id = id;
        this.title = title;
        this.titleAscii = titleAscii;
        this.source = source;
        this.status = status;
        this.story = story;
        this.birthPlace = birthPlace;
        this.birthPlaceAscii = birthPlaceAscii;
        this.livingPlace = livingPlace;
        this.livingPlaceAscii = livingPlaceAscii;
        this.date = date;
    }

    public String toString() {
        return this.title;
    }

    public static List<Post> getPostsFilteredByNameAndPlace(String name, String place) {
        String query ;
        //query = "SELECT * FROM POST";
        List<Post> res = new ArrayList<>();
        Log.e("Post","name: "+name);
        Log.e("Post","place: "+place);
        name = ""+ DataLoad.toAscii(name)+"";
        place = ""+DataLoad.toAscii(place)+"";

        //return Post.findWithQuery(Post.class,query);
        if(name != null && !name.isEmpty()) {
            if(place != null && !place.isEmpty()) {
                query = "title_ascii LIKE ? AND (birth_place_ascii LIKE ? OR living_place_ascii LIKE ?)";
                res = Post.find(Post.class, query, "%"+name+"%", "%"+place+"%", "%"+place+"%");
                Log.e("Post res 1: ", res.size()+"");
            } else {
                query = "title_ascii LIKE ? ";
                res = Post.find(Post.class, query, "%"+name+"%");
                Log.e("Post res: 2", res.size()+"");
            }
        } else {
            if(place != null && !place.isEmpty()) {
                query = "birth_place_ascii LIKE ? OR living_place_ascii LIKE ? ";
                res = Post.find(Post.class, query, "%"+place+"%", "%"+place+"%");
                Log.e("Post res: 3", res.size()+"");
            }
        }
        return res;
    }


}
